package com.team2.trainingprogramrepo.config;

import com.fasterxml.jackson.databind.util.StdConverter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Date;
import java.text.SimpleDateFormat;


public class SqlDateConverter extends StdConverter<String, Date> {
    private static final Logger LOGGER = LogManager.getLogger(SqlDateConverter.class);

    @Override
    public Date convert(String value) {
        try {
            SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy/MM/dd");
            SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date parsedDate = inputFormat.parse(value);
            String formattedDate = outputFormat.format(parsedDate);
            return Date.valueOf(formattedDate);
        } catch (Exception e) {
            LOGGER.error("Convert Date Failed");
            return null;
        }
    }
}