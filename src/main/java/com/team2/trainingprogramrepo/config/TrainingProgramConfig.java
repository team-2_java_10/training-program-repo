package com.team2.trainingprogramrepo.config;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@Configuration
public class TrainingProgramConfig {
    private static final Logger LOGGER = LogManager.getLogger(TrainingProgramConfig.class);

    @Value("${class-service-url}")
    private String classServiceUrl;

    @Value("${syllabus-service-url}")
    private String syllabusServiceUrl;

    @Value("${date-format}")
    private String dateFormat;

    @Value("${user-service-url}")
    private String userUrl;

    @Bean
    @LoadBalanced
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean
    public Properties properties() {
        Properties properties = new Properties();

        try {
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream("application.yml");
            properties.load(inputStream);
        } catch (IOException e) {
            LOGGER.error("Properties load failed");
        }

        properties.setProperty("class-service-url", classServiceUrl);
        properties.setProperty("syllabus-service-url", syllabusServiceUrl);
        properties.setProperty("user-url", userUrl);
        properties.setProperty("date-format", dateFormat);

        return properties;
    }
}
